# Carbon emissions from Austrian health care between 2005 and 2015

Companion repository for manuscript "Carbon emissions from Austrian health care between 2005 and 2015" submitted to **Resources, Conservation
and Recycling**, November 3, 2019

This repository will include reproducible RMarkdown documents for text, figures and data once the manuscript is published. At present, it only includes the SI data file (AUTHCF SI_data.xlsx).